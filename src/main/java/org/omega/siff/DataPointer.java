package org.omega.siff;

public class DataPointer {

    private long offset;
    private int dataLength;

    public DataPointer() {
    }

    public DataPointer(long offset, int dataLength) {
        this.offset = offset;
        this.dataLength = dataLength;
    }

    public long getOffset() {
        return offset;
    }

    public int getDataLength() {
        return dataLength;
    }
}