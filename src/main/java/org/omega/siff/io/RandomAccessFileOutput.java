package org.omega.siff.io;

import com.esotericsoftware.kryo.KryoException;
import com.esotericsoftware.kryo.io.Output;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class RandomAccessFileOutput extends Output {

    private RandomAccessFile raf;

    public RandomAccessFileOutput(String filename) throws FileNotFoundException {
        this(new File(filename));
    }

    public RandomAccessFileOutput(File file) throws FileNotFoundException {
        super(4096);

        raf = new RandomAccessFile(file, "rw");
    }

    public void seek(long position) throws KryoException {
        try {
            raf.seek(position);
            reset();
        } catch (IOException e) {
            throw new KryoException(e);
        }
    }

    public void setLength(long fileLength) throws KryoException {
        try {
            raf.setLength(fileLength);
        } catch (IOException e) {
            throw new KryoException(e);
        }
    }

    public long getPointer() throws KryoException {
        try {
            return raf.getFilePointer();
        } catch (IOException e) {
            throw new KryoException(e);
        }
    }

    @Override
    public void flush() throws KryoException {
        try {
            raf.write(buffer, 0, position);
        } catch (IOException e) {
            throw new KryoException(e);
        }

        total += position;
        position = 0;
    }

    @Override
    public void close() throws KryoException {
        try {
            raf.close();
        } catch (IOException e) {
            throw new KryoException(e);
        }
    }
}