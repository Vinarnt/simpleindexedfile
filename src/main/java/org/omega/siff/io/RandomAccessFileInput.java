package org.omega.siff.io;

import com.esotericsoftware.kryo.KryoException;
import com.esotericsoftware.kryo.io.Input;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class RandomAccessFileInput extends Input {

    private final RandomAccessFile raf;

    public RandomAccessFileInput(String filename) throws FileNotFoundException {
        this(new File(filename));
    }

    public RandomAccessFileInput(File file) throws FileNotFoundException {
        super(4096);

        raf = new RandomAccessFile(file, "r");
    }

    @Override
    protected int fill(byte[] buffer, int offset, int count) throws KryoException {
        try {
            return raf.read(buffer, offset, count);
        } catch (IOException e) {
            throw new KryoException(e);
        }
    }

    public void seek(long position) throws KryoException {
        try {
            raf.seek(position);
            setPosition(limit);
        } catch (IOException e) {
            throw new KryoException(e);
        }
    }

    public long getPointer() throws KryoException {
        try {
            return raf.getFilePointer();
        } catch (IOException e) {
            throw new KryoException(e);
        }
    }

    @Override
    public void close() throws KryoException {
        try {
            raf.close();
        } catch (IOException e) {
            throw new KryoException(e);
        }
    }
}