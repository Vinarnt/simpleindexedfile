package org.omega.siff.header;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import org.omega.siff.DataPointer;
import org.omega.siff.index.Index;

@SuppressWarnings("rawtypes")
public class HeaderSerializer extends Serializer<Header> {

    public HeaderSerializer() {
    }

    @Override
    public void write(Kryo kryo, Output output, Header object) {
        kryo.writeClassAndObject(output, object.getIndex());
        kryo.writeObject(output, object.getDataPointer());
    }

    @Override
    public Header read(Kryo kryo, Input input, Class<? extends Header> type) {
        Index index = (Index) kryo.readClassAndObject(input);
        DataPointer dataPointer = kryo.readObject(input, DataPointer.class);

        return new Header(index, dataPointer);
    }
}