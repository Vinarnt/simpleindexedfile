package org.omega.siff.header;

import org.omega.siff.DataPointer;
import org.omega.siff.index.Index;

public class Header {

    private long headerOffset;
    private int headerLength;

    private Index<?> index;
    private DataPointer dataPointer;
    private boolean free;

    public Header() {
    }

    public Header(Index<?> index, DataPointer dataPointer) {
        this(index, dataPointer, false);
    }

    public Header(Index<?> index, DataPointer dataPointer, boolean free) {
        this.index = index;
        this.dataPointer = dataPointer;
        this.free = free;
    }

    public long getHeaderOffset() {
        return headerOffset;
    }

    public void setHeaderOffset(long headerPointer) {
        this.headerOffset = headerPointer;
    }

    public int getHeaderLength() {
        return headerLength;
    }

    public void setHeaderLength(int headerLength) {
        this.headerLength = headerLength;
    }

    public Index<?> getIndex() {
        return index;
    }

    public void setIndex(Index<?> index) {
        this.index = index;
    }

    public DataPointer getDataPointer() {
        return dataPointer;
    }

    public void setDataPointer(DataPointer dataPointer) {
        this.dataPointer = dataPointer;
    }

    public boolean isFree() {
        return free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }

    @Override
    public String toString() {
        return "Header[ " + index.toString() + ", " + dataPointer + ", " + free + "]";
    }
}