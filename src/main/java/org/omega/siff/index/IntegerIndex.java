package org.omega.siff.index;

public class IntegerIndex extends Index<Integer> {

    public IntegerIndex() {
    }

    public IntegerIndex(int value) {
	super(value);
    }

    @Override
    protected boolean indexEquals(Index<Integer> index) {
	return index.value.equals(value);
    }

    @Override
    protected int indexHashCode() {
	return value;
    }
}