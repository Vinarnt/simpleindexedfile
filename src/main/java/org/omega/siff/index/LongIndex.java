package org.omega.siff.index;

public class LongIndex extends Index<Long> {

    public LongIndex() {
    }

    public LongIndex(long value) {
	super(value);
    }

    @Override
    protected boolean indexEquals(Index<Long> index) {
	return index.value.equals(value);
    }

    @Override
    protected int indexHashCode() {
	return value.hashCode();
    }
}