package org.omega.siff.index;

public class StringIndex extends Index<String> {

    public StringIndex() {
    }

    public StringIndex(String index) {
        super(index);
    }

    @Override
    protected boolean indexEquals(Index<String> index) {
        StringIndex stringIndex = (StringIndex) index;
        return stringIndex.value.equals(this.value);
    }

    @Override
    protected int indexHashCode() {
        return value.hashCode();
    }
}