package org.omega.siff.index;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

@SuppressWarnings("rawtypes")
public class IndexSerializer<T extends Index> extends Serializer<T> {

    public IndexSerializer() {
    }

    @Override
    public void write(Kryo kryo, Output output, T object) {
        kryo.writeClassAndObject(output, object.value);
    }

    @SuppressWarnings("unchecked")
    @Override
    public T read(Kryo kryo, Input input, Class<? extends T> type) {
        Object value = kryo.readClassAndObject(input);

        T index = null;
        try {
            index = type.newInstance();
            index.value = value;
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return index;
    }
}