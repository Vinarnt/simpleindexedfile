package org.omega.siff.index;

public abstract class Index<T> {

    protected final static int registrationId = -1;

    protected T value;

    public Index() {
    }

    public Index(T value) {
        this.value = value;
    }

    protected abstract boolean indexEquals(Index<T> index);

    protected abstract int indexHashCode();

    @SuppressWarnings("unchecked")
    @Override
    public final boolean equals(Object object) {
        return object != null && object.getClass() == this.getClass() && indexEquals((Index<T>) object);
    }

    @Override
    public final int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + registrationId;
        result = prime * result + indexHashCode();

        return result;
    }
}