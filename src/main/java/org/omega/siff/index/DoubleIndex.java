package org.omega.siff.index;

public class DoubleIndex extends Index<Double> {

    public DoubleIndex() {
    }

    public DoubleIndex(double value) {
        super(value);
    }

    @Override
    protected boolean indexEquals(Index<Double> index) {
        return index.value.equals(value);
    }

    @Override
    protected int indexHashCode() {
        return value.hashCode();
    }
}