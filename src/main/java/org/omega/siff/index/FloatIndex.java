package org.omega.siff.index;

public class FloatIndex extends Index<Float> {

    public FloatIndex() {
    }

    public FloatIndex(float value) {
	super(value);
    }

    @Override
    protected boolean indexEquals(Index<Float> index) {
	return index.value.equals(value);
    }

    @Override
    protected int indexHashCode() {
	return value.hashCode();
    }
}