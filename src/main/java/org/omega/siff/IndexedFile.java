package org.omega.siff;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoException;
import com.esotericsoftware.kryo.Registration;
import com.esotericsoftware.kryo.io.ByteBufferOutput;
import org.omega.siff.header.Header;
import org.omega.siff.header.HeaderSerializer;
import org.omega.siff.index.*;
import org.omega.siff.io.RandomAccessFileInput;
import org.omega.siff.io.RandomAccessFileOutput;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;

public class IndexedFile {

    private Kryo kryo;
    private ByteBufferOutput binaryOutput;
    private RandomAccessFileOutput fileOutput;
    private RandomAccessFileInput fileInput;

    private Map<Index<?>, Header> headers;
    private List<Header> freeSpaces;

    private HeaderLengthComparator headerLengthComparator;
    private DataLengthComparator dataLengthComparator;
    private DataPointerComparator dataPointerComparator;

    private Header lastHeader;
    private DataPointer lastData;

    private static final int RESERVED_HEADERS_CHUNKS_LENGTH = 8192;
    private static final int MAX_HEADER_SPACE_LOSS = 16;
    private static final int MAX_DATA_SPACE_LOSS = 64;

    private static final int HEADER_COUNT_OFFSET = 0;
    private static final int DATA_REGION_POINTER_OFFSET = HEADER_COUNT_OFFSET + (Long.SIZE / 8);
    private static final int HEADERS_REGION_OFFSET = DATA_REGION_POINTER_OFFSET + (Long.SIZE / 8);

    private long dataRegionOffset;

    public IndexedFile(String filename) throws IOException {
        this(new File(filename));
    }

    IndexedFile(File file) throws IOException {
        boolean newFile = !file.exists();

        headers = new HashMap<>();
        freeSpaces = new ArrayList<>();

        headerLengthComparator = new HeaderLengthComparator();
        dataLengthComparator = new DataLengthComparator();
        dataPointerComparator = new DataPointerComparator();

        kryo = new Kryo();
        // Log.DEBUG = true;
        // Log.INFO = true;
        // Log.DEBUG = true;
        // Log.ERROR = true;
        // Log.WARN = true;
        // Log.TRACE = true;
        kryo.setReferences(false);
        kryo.setRegistrationRequired(true);
        kryo.register(IntegerIndex.class, new IndexSerializer<IntegerIndex>(), 11);
        kryo.register(LongIndex.class, new IndexSerializer<LongIndex>(), 12);
        kryo.register(FloatIndex.class, new IndexSerializer<FloatIndex>(), 13);
        kryo.register(DoubleIndex.class, new IndexSerializer<DoubleIndex>(), 14);
        kryo.register(StringIndex.class, new IndexSerializer<StringIndex>(), 15);
        kryo.register(Header.class, new HeaderSerializer(), 20);
        kryo.register(DataPointer.class, 21);

        binaryOutput = new ByteBufferOutput(4096);
        fileOutput = new RandomAccessFileOutput(file);
        fileInput = new RandomAccessFileInput(file);

        if (newFile) {
            try {
                createFile();
            } catch (IOException e) {
                throw new IOException("Error when writing the metadata", e);
            }
        } else {
            readHeaders();
        }
    }

    private void createFile() throws IOException {
        dataRegionOffset = HEADERS_REGION_OFFSET + RESERVED_HEADERS_CHUNKS_LENGTH;

        // Write metadata
        fileOutput.writeLong(0L); // Number of headers
        fileOutput.writeLong(dataRegionOffset);

        // Write the headers region
        byte[] headersRegion = new byte[RESERVED_HEADERS_CHUNKS_LENGTH];
        Arrays.fill(headersRegion, (byte) 0);
        fileOutput.write(headersRegion);

        fileOutput.flush();
    }

    private void readHeaders() {
        long headersCount = fileInput.readLong();
        System.out.println("Headers count : " + headersCount);

        dataRegionOffset = fileInput.readLong();
        System.out.println("Data region offset : " + dataRegionOffset);

        long lastDataOffset = 0L;
        Header header = null;
        for (int i = 0; i < headersCount; i++) {
            long startPointer = fileInput.getPointer();
            header = kryo.readObject(fileInput, Header.class);
            header.setHeaderOffset(startPointer - HEADERS_REGION_OFFSET);
            header.setHeaderLength((int) (fileInput.getPointer() - startPointer));

            if (header.isFree()) {
                freeSpaces.add(header);
            }

            long dataOffset = header.getDataPointer().getOffset();
            if (dataOffset > lastDataOffset) {
                lastDataOffset = dataOffset;
                lastData = header.getDataPointer();
            }

            headers.put(header.getIndex(), header);
        }

        lastHeader = header;
    }

    public <T extends Index<?>> Registration registerIndex(Class<T> type) {
        Registration registration = kryo.register(type);
        try {

            Field registrationIdField = type.getField("registrationId");
            registrationIdField.setAccessible(true);
            registrationIdField.setInt(null, registration.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return registration;
    }

    private void ensureHeadersCapacity(long ensureLength) {
        if (lastHeader == null) {
            return;
        }

        long endOfLastHeader = HEADERS_REGION_OFFSET + lastHeader.getHeaderOffset() + lastHeader
                .getHeaderLength();
        if (dataRegionOffset - endOfLastHeader >= ensureLength) {
            return;
        }

        Collections.sort(freeSpaces, dataPointerComparator);

        long remaining = RESERVED_HEADERS_CHUNKS_LENGTH;
        fileOutput.seek(dataRegionOffset + lastData.getOffset() + lastData.getDataLength());
        for (int i = 0; i < freeSpaces.size() && remaining > 0L; i++) {
            Header freeHeader = freeSpaces.get(i);
            DataPointer dataPointer = freeHeader.getDataPointer();
            if (dataPointer == null) {
                continue;
            }

            fileInput.seek(dataRegionOffset + dataPointer.getOffset());
            byte[] dataBytes = fileInput.readBytes(dataPointer.getDataLength());

            fileOutput.writeBytes(dataBytes);
            lastData = dataPointer;

            remaining -= dataBytes.length;
        }
        fileOutput.flush();
    }

    private <T extends Index> Header getFreeHeaderAndDataSpace(T index, int headerLength, int dataLength) {

        boolean newHeader = false;
        boolean newDataSpace = false;

        Header freeHeader = getFreeAvailableHeader(headerLength);
        if (freeHeader == null) {
            freeHeader = getFreeHeader(headerLength);
            newHeader = true;
        }

        DataPointer dataSpace = getFreeAvailableDataSpace(dataLength);
        if (dataSpace == null) {
            dataSpace = getFreeDataSpace(dataLength);
            newDataSpace = true;
        }

        freeHeader.setIndex(index);
        freeHeader.setDataPointer(dataSpace);

        if (newHeader) {
            fileOutput.seek(HEADERS_REGION_OFFSET + freeHeader.getHeaderOffset());
            kryo.writeObject(fileOutput, freeHeader);
            fileOutput.flush();

            lastHeader = freeHeader;
        }

        if (newDataSpace) {
            lastData = dataSpace;
        }

        return freeHeader;
    }

    private Header getFreeAvailableHeader(int minLength) {
        Collections.sort(freeSpaces, headerLengthComparator);

        for (int i = 0; i < freeSpaces.size(); i++) {
            Header freeHeader = freeSpaces.get(i);
            long headerLength = freeHeader.getHeaderLength();
            if (headerLength >= minLength && (headerLength - minLength) <= MAX_HEADER_SPACE_LOSS) {
                freeSpaces.remove(i);
                freeHeader.setFree(false);

                return freeHeader;
            }
        }

        return null;
    }

    private Header getFreeHeader(int headerLength) {
        ensureHeadersCapacity(headerLength);

        long newHeaderOffset;
        if (lastHeader == null)
            newHeaderOffset = 0;
        else
            newHeaderOffset = lastHeader.getHeaderOffset() + lastHeader
                    .getHeaderLength();

        Header newHeader = new Header();
        newHeader.setHeaderOffset(newHeaderOffset);
        newHeader.setHeaderLength(headerLength);

        return newHeader;
    }

    private DataPointer getFreeAvailableDataSpace(int minLength) {
        Collections.sort(freeSpaces, dataLengthComparator);

        for (Header freeSpace : freeSpaces) {
            long dataLength = freeSpace.getDataPointer().getDataLength();
            if (dataLength >= minLength && (dataLength - minLength) <= MAX_DATA_SPACE_LOSS) {
                DataPointer dataPointer = freeSpace.getDataPointer();
                freeSpace.setDataPointer(null);

                return dataPointer;
            }
        }

        return null;
    }

    private DataPointer getFreeDataSpace(int dataLength) {
        long newDataSpaceOffset;
        if (lastData == null)
            newDataSpaceOffset = 0;
        else
            newDataSpaceOffset = lastData.getOffset() + lastData
                    .getDataLength();

        return new DataPointer(newDataSpaceOffset, dataLength);
    }

    private <T> Header findHeader(Index<T> index) {
        return headers.get(index);
    }

    public <T> void set(Index<T> index, Object object) throws IOException {
        if (index == null) {
            throw new NullPointerException("Index should not be null");
        }

        // Find the header
        Header header = findHeader(index);
        if (header == null) { // Create header
            try {
                // Get data to binary
                kryo.writeObject(binaryOutput, object);
                byte[] dataBytes = binaryOutput.toBytes();
                int dataLength = dataBytes.length;
                binaryOutput.reset();

                // Get header to binary
                // TODO: Define length for each index
                header = new Header(index, new DataPointer(0L, dataLength));
                kryo.writeObject(binaryOutput, header);
                byte[] headerBytes = binaryOutput.toBytes();
                int headerLength = headerBytes.length;
                binaryOutput.reset();

                // Get free data space and header
                header = getFreeHeaderAndDataSpace(index, headerLength, dataLength);
                header.setIndex(index);

                // Write the data
                fileOutput.seek(dataRegionOffset + header.getDataPointer().getOffset());
                fileOutput.writeBytes(dataBytes);
                fileOutput.flush();
            } catch (KryoException e) {
                throw new IOException("Unable to create header", e);
            }

            headers.put(index, header);

            // Update header count
            fileOutput.seek(HEADER_COUNT_OFFSET);
            fileOutput.writeLong(headers.size());
            fileOutput.flush();
        } else {
            System.out.println("Replace index");
        }
    }

    public <T> T get(Index<?> index, Class<T> type) {
        // TODO: infer the type from the index
        if (index == null) {
            throw new NullPointerException("Index should not be null");
        }

        Header header = findHeader(index);
        if (header == null) {
            return null;
        }

        DataPointer dataPointer = header.getDataPointer();
        try {
            fileInput.seek(dataRegionOffset + dataPointer.getOffset());
            T value = kryo.readObject(fileInput, type);

            return value;
        } catch (KryoException e) {
            e.printStackTrace();
        }

        return null;
    }

    public <T> void remove(Index<T> index) throws KryoException {
        if (index == null) {
            throw new NullPointerException("Index should not be null");
        }

        Header header = findHeader(index);
        if (header == null) {
            return;
        }

        freeSpaces.add(header);

        DataPointer dataPointer = header.getDataPointer();
        // TODO:

    }

    void close() {
        fileInput.close();
        binaryOutput.close();
    }
}