package org.omega.siff;

import org.omega.siff.header.Header;

import java.util.Comparator;

public class DataLengthComparator implements Comparator<Header> {

    public DataLengthComparator() {
    }

    public int compare(Header o1, Header o2) {
        long l1 = o1.getDataPointer().getDataLength();
        long l2 = o2.getDataPointer().getDataLength();

        if (l1 < l2) {
            return -1;
        } else if (l1 > l2) {
            return 1;
        }

        return 0;
    }
}