package org.omega.siff;

import org.junit.Assert;
import org.junit.Test;
import org.omega.siff.index.IntegerIndex;
import org.omega.siff.index.LongIndex;
import org.omega.siff.index.StringIndex;

public class IndexTest {

    public IndexTest() {
    }

    @Test
    public void integerIndexTest() {
        IntegerIndex index1 = new IntegerIndex(Integer.MAX_VALUE);
        IntegerIndex index2 = new IntegerIndex(Integer.MAX_VALUE);

        Assert.assertEquals(index1, index2);

        index1 = new IntegerIndex(Integer.MAX_VALUE);
        index2 = new IntegerIndex(Integer.MAX_VALUE - 1);

        Assert.assertNotEquals(index1, index2);
    }

    @Test
    public void longIndexTest() {
        LongIndex index1 = new LongIndex(Long.MAX_VALUE);
        LongIndex index2 = new LongIndex(Long.MAX_VALUE);

        Assert.assertEquals(index1, index2);

        index1 = new LongIndex(Long.MAX_VALUE);
        index2 = new LongIndex(Long.MAX_VALUE - 1);

        Assert.assertNotEquals(index1, index2);
    }

    @Test
    public void objectIndexTest() {
        StringIndex index1 = new StringIndex("some string");
        StringIndex index2 = new StringIndex("some string");

        Assert.assertEquals(index1, index2);

        index1 = new StringIndex("some string");
        index2 = new StringIndex("some other string");

        Assert.assertNotEquals(index1, index2);
    }
}