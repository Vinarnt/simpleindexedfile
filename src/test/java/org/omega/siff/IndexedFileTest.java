package org.omega.siff;

import org.junit.Assert;
import org.junit.Test;
import org.omega.siff.index.*;

import java.io.File;
import java.io.IOException;

public class IndexedFileTest {

    public IndexedFileTest() {
    }

    private IndexedFile openIndexedFile(String filename, boolean deleteOnExists)
            throws IOException {
        File file = new File(filename);
        if (deleteOnExists && file.exists()) {
            file.delete();
        }

        return new IndexedFile(file);
    }

    @Test
    public void createIndexedFile() throws IOException {
        IndexedFile indexedFile = openIndexedFile("createdIndexedFile", true);
        indexedFile.close();
    }

    @Test
    public void writeInIndexedFile() throws IOException {
        IndexedFile indexedFile = openIndexedFile("writedIndexedFile", true);
        try {
            indexedFile.set(new StringIndex("stringIndex"), "StringIndexValue");
            indexedFile.set(new IntegerIndex(1), "IntegerIndexValue");
            indexedFile.set(new LongIndex(1L), "LongIndexValue");
            indexedFile.set(new FloatIndex(1.5f), "FloatIndexValue");
            indexedFile.set(new DoubleIndex(1.5d), "DoubleIndexValue");
        } finally {
            indexedFile.close();
        }
    }

    @Test
    public void readFromIndexedFile() throws IOException {
        IndexedFile indexedFile = openIndexedFile("writedIndexedFile", false);
        try {
            String integerValue = indexedFile.get(new IntegerIndex(1), String.class);
            Assert.assertTrue(integerValue != null && integerValue.equals("IntegerIndexValue"));

            String longValue = indexedFile.get(new LongIndex(1L), String.class);
            Assert.assertTrue(longValue != null && longValue.equals("LongIndexValue"));

            String floatValue = indexedFile.get(new FloatIndex(1.5f), String.class);
            Assert.assertTrue(floatValue != null && floatValue.equals("FloatIndexValue"));

            String doubleValue = indexedFile.get(new DoubleIndex(1.5d), String.class);
            Assert.assertTrue(doubleValue != null && doubleValue.equals("DoubleIndexValue"));

            String stringIndex = indexedFile.get(new StringIndex("stringIndex"), String.class);
            Assert.assertTrue(stringIndex != null && stringIndex.equals("StringIndexValue"));
        } finally {
            indexedFile.close();
        }
    }

    // @Test
    public void removeFromIndexedFile() throws IOException {
        IndexedFile indexedFile = openIndexedFile("removeIndexedFile", false);
        try {
            String value = indexedFile.get(new IntegerIndex(1), String.class);
            System.out.println(value);
            Assert.assertFalse(value == null);

            indexedFile.remove(new IntegerIndex(1));

            value = indexedFile.get(new IntegerIndex(1), String.class);
            Assert.assertTrue(value == null);
        } finally {
            indexedFile.close();
        }
    }
}